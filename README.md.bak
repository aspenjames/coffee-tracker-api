# Coffee Tracker

This is the back-end repository for "Coffee Tracker" - an application that
allows users to store and track their pour-over coffee recipes. This is a Rails
API server, the front-end code can be found
[here](https://gitlab.com/aspenjames/coffee-tracker-web).

## Dependencies

Built using Ruby version 2.6.0 and Rails version 5.2.3. The database technology
used is PostgreSQL, with secure password hashing accomplished by BCrypt. Other
dependencies can be found in the `Gemfile`.

## Installation

You may set up your own server to run locally (on your own machine) or on a
personal server if desired. To start, fork and clone this repository to the
destination machine.

### Prerequisites

- [Ruby](https://www.ruby-lang.org/) version 2.6.x
- [Bundler](https://bundler.io/)
- [PostgreSQL](https://www.postgresql.org/)

### Installing Dependencies

All dependencies can be installed by issuing `bundle install` from the root
directory of the project. If you encounter error messages related to the Bundler
version, then the easiest course of action is to remove the lockfile with
`rm Gemfile.lock`, then execute `bundle install` again.

### Environment Variables

Encoding the JSON Web Token used for authorization requires an environment
variable (JWT_SECRET) to be set. You may set this manually, or create a file in
the root directory named `.env` (the dot preceeding it is important!) and adding
the following line:


```
JWT_SECRET=superSecretSecureStringHere0123$$
```

Substitute everything beyond the equals sign with a random string.

If you are curious how the `.env` file works, then visit
[dotenv-rails](https://rubygems.org/gems/dotenv-rails) for more information!

### Setting Up the Database

The PostgreSQL database configuration file is located at `config/database.yml`
The default configuration is:
TODO: Input default config info

Once you are satisfied with the configuration, use these to set up the database:


```
rails db:create
rails db:migrate
```

Optionally, you may seed the database with some starter data. The starter data
can be found in `db/seeds.rb`, and you can run this with `rails db:seed`.

### Starting the Server

Once you have the above steps completed, run the server with `rails server`.
This defaults to running on localhost, port 3000. You may specify another port
with `rails server -p <port number>`.

### API Endpoints

You may view all of the endpoints with `rails routes`, and see where these are
defined in `config/routes.rb`. These are the main endpoints used by the front-
end application:
TODO: Fill out common routes
- `/login` => user login auth endpoint, returns an error message or a user and
their associated JSON Web Token
- `/signup` => user creation, returns an error message or a new user and their
associated JSON Web Token

## Miscellany

### Why do users have no names or emails?

I personally feel that most web applications require too much personally
identifying information (PII). This is made to be a way for users to keep and
share their pour-over coffee journals and recipes, and I don't believe that
requires acquiring and storing PII. Users may elect to use their name or a
commonly used, recognizable usernames while others may use an alias that does
not reveal any information about themselves. I believe the end users should have
enough freedom to decide how much PII they would like to disclose when using my
application.

### I would love to see feature XYZ

Amazing! I would love to hear about that! The best way to get that in front of
me (and others who may be able to help) would be to file an issue on the GitLab
repository. Please read the [contributing guidelines](https://gitlab.com/aspenjames/coffee-tracker-web/blob/master/CONTRIBUTING.md).
That is also the perfect place to start if you can program and would like to
contribute - pull requests are welcome and encouraged.

