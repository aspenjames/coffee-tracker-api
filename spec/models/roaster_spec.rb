require 'rails_helper'

RSpec.describe Roaster, type: :model do
	before(:all) do
		Roaster.destroy_all
		@roaster = Roaster.create(:name => "Broadcast Coffee Roasters")
	end

	it "can be created with a name" do
		expect(@roaster).to be_valid
	end

	it "responds to `#name`" do
		expect(@roaster.name).to eq("Broadcast Coffee Roasters")
	end

	it "cannot create two roasters with the same name" do
		r1 = Roaster.create(:name => 'Broadcast Coffee Roasters')

		expect(r1).not_to be_valid
	end
end
