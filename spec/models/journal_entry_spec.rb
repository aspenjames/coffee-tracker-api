require 'rails_helper'

RSpec.describe JournalEntry, type: :model do
	before(:all) do
		BrewMethod.destroy_all
		Coffee.destroy_all
		JournalEntry.destroy_all
		Roaster.destroy_all
		User.destroy_all
		
		@u = User.create({
			:username => "jane_doe",
			:password => "password"
		})

		@r = Roaster.create(:name => "Broadcast Coffee Roasters")

		@m = BrewMethod.create({
			:name => "Hario V60",
			:filter_type => "Paper"
		})

		@c = Coffee.create({
			:country => "Guatemala",
			:name => "La Bolsa",
			:roaster => @r
		})

		@je_attributes = {
			:user => @u,
			:coffee => @c,
			:brew_method => @m,
			:dose => 25,
			:brew_weight => 400,
			:time => 180,
			:days_since_roast => 4,
			:notes => "Amazing cup of coffee, maybe bordering on over-extraction. Might grind a little coarser next time.",
			:share => true
		}

		@je = JournalEntry.create(@je_attributes)
	end

	it "can be created with proper attributes" do
		expect(@je).to be_valid
	end

	it "cannot be created without a user, coffee, or brew method" do
		je1 = JournalEntry.create(@je_attributes.merge(:user => nil))
		je2 = JournalEntry.create(@je_attributes.merge(:coffee => nil))
		je3 = JournalEntry.create(@je_attributes.merge(:brew_method => nil))

		expect(je1).not_to be_valid
		expect(je2).not_to be_valid
		expect(je3).not_to be_valid
	end

	it "cannot be created without a dose" do
		je = JournalEntry.create(@je_attributes.merge(:dose => nil))

		expect(je).not_to be_valid
	end

	it "cannot be created without a brew_weight" do
		je = JournalEntry.create(@je_attributes.merge(:brew_weight => nil))

		expect(je).not_to be_valid
	end

	it "cannot be created without a time" do
		je = JournalEntry.create(@je_attributes.merge(:time => nil))

		expect(je).not_to be_valid
	end

	it "defaults days_since_roast to 'unknown' if not supplied or 0" do
		je = JournalEntry.create(@je_attributes.merge(:days_since_roast => nil))

		expect(je.days_since_roast).to eq("unknown")
	end

	it "has a default value of 'false' for :share if not supplied" do
		je_atts = @je_attributes.clone
		je_atts.delete(:share)
		je = JournalEntry.create(je_atts)

		expect(je.share).to be(false)
	end

	it "responds to #coffee and returns an instance of the Coffee class" do
		expect(@je.coffee).to be_an_instance_of(Coffee)
		expect(@je.coffee).to be(@c)
	end

	it "responds to #user and returns an instance of the User class" do
		expect(@je.user).to be_an_instance_of(User)
		expect(@je.user).to be(@u)
	end

	it "responds to #brew_method and returns an instance of the BrewMethod class" do
		expect(@je.brew_method).to be_an_instance_of(BrewMethod)
		expect(@je.brew_method).to be(@m)
	end
end
