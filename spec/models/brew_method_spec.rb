require 'rails_helper'

RSpec.describe BrewMethod, type: :model do
	before(:all) do
		BrewMethod.destroy_all
		@b = BrewMethod.create({
			:name => "Hario V60",
			:filter_type => "Paper"
		})
	end

	it "can be created with valid attributes" do
		expect(@b).to be_valid
	end

	it "cannot be created without a name" do
		b1 = BrewMethod.create(:filter_type => "Metal")

		expect(b1).not_to be_valid
	end

	it "cannot be created without a filter_type" do
		b2 = BrewMethod.create(:name => "Kalita Wave")

		expect(b2).not_to be_valid
	end

	it "can only have a filter_type of 'Paper', 'Metal', 'Cloth', or 'None'" do
		b3 = BrewMethod.create({
			:name => "Invalid Brew Method",
			:filter_type => "plastic"
		})

		expect(b3).not_to be_valid
	end

	it "capitalizes filter_type when read, despite how it is stored" do
		b4 = BrewMethod.create({
			:name => "French Press",
			:filter_type => "METAL"
		})

		b5 = BrewMethod.create({
			:name => "Oji",
			:filter_type => "paper"
		})

		b6 = BrewMethod.create({
			:name => "Clever",
			:filter_type => "cLoTh"
		})

		expect(b4.filter_type).to eq("Metal")
		expect(b5.filter_type).to eq("Paper")
		expect(b6.filter_type).to eq("Cloth")
	end
end
