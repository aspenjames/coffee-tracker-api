require 'rails_helper'

RSpec.describe User, type: :model do
	before(:all) do
		User.destroy_all
		@user = User.create({
			:username => 'jane_doe',
			:password => 'password'
		})
	end

	it "can be created with a valid username and password" do
		expect(@user).to be_valid
	end

	it "responds to `.username` and authenticates valid password" do
		expect(@user.username).to eq('jane_doe')
		expect(@user.authenticate('password')).to eq(@user)
	end

	it "will not authenticate an invalid password" do
		expect(@user.authenticate('not_password')).to be_falsy
	end

	it "cannot create two users with the same username" do
		user = User.create({
			:username => 'jane_doe',
			:password => 'password'
		})

		expect(user).not_to be_valid
	end

	it "cannot be created without a username" do
		user = User.create(:password => 'password')

		expect(user).not_to be_valid
	end

	it "cannot be created without a password" do
		user = User.create(:username => 'john_doe')

		expect(user).not_to be_valid
	end
end
