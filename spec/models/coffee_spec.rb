require 'rails_helper'

RSpec.describe Coffee, type: :model do
	before(:all) do
		Roaster.destroy_all
		Coffee.destroy_all

		@r = Roaster.create(:name => "Broadcast Coffee Roasters")
		@coffee_attributes = {
			:country =>"Guatemala",
			:name => "La Bolsa",
			:roaster => @r
		}
		@c = Coffee.create(@coffee_attributes)
	end

	it "can be created with valid attributes" do
		expect(@c).to be_valid
	end

	it "cannot be created without a country or name" do
		no_country = @coffee_attributes.clone.tap{|s| s.delete(:country)}
		no_name = @coffee_attributes.clone.tap{|s| s.delete(:name)}

		c1 = Coffee.create(no_country)
		c2 = Coffee.create(no_name)

		expect(c1).not_to be_valid
		expect(c2).not_to be_valid
	end

	it "cannot be created without a roaster" do
		no_roaster = @coffee_attributes.clone.tap{|s| s.delete(:roaster)}
		
		c1 = Coffee.create(no_roaster)
		
		expect(c1).not_to be_valid
	end

	it "responds to #roaster and returns an instance of the Roaster class" do
		expect(@c.roaster).to be_an_instance_of(Roaster)
		expect(@c.roaster).to be(@r)
	end
end
