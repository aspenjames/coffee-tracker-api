class Api::V1::UsersController < ApplicationController
  before_action :set_user, :only => %i(show update)

	def create
		@user = User.new(user_params)

		if @user.save
			token = encode_token({ :user_id => @user.id })
			render :json => { :user => UserSerializer.new(@user), :jwt => token }
		else
			render :json => { :message => @user.errors.full_messages.join(', ') }
		end
  end

  def show
    render :json => @user
  end

  def update
  end

  def index
    render :json => User.all
  end

  private
  def set_user
    @user = User.find(params[:id])
	end

	def user_params
		params.permit(:username, :password)
	end
end
