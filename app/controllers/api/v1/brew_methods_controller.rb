class Api::V1::BrewMethodsController < ApplicationController
  before_action :set_brew_method, :only => %i(show update)
  
  def create
  end
  
  def show
    render :json => @brew_method
  end

  def update
  end

  def index
    render :json => BrewMethod.all
  end

  private
  def set_brew_method
    @brew_method = BrewMethod.find(params[:id])
  end
end
