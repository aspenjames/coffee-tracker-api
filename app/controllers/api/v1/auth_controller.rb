class Api::V1::AuthController < ApplicationController
	def create
		@user =  User.find_by(:username => params[:username])

		if @user && @user.authenticate(params[:password])
			token = encode_token({ :user_id => @user.id })
			render :json => { :user => UserSerializer.new(@user), :jwt => token }
		else
			render :json => { :message => 'invalid username and/or password' }
		end
	end
end