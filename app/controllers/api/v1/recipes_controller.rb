class Api::V1::RecipesController < ApplicationController
  before_action :set_recipe, :only => %i(show update)
  
  def create
  end

  def show
    render :json => @recipe
  end

  def update
  end

  def index
    render :json => Recipe.all
  end

  private
  def set_recipe
    @recipe = Recipe.find(params[:id])
  end
end
