class Api::V1::CoffeesController < ApplicationController
  before_action :set_coffee, :only => %i(show update)
  
  def create
  end

  def show
    render :json => @coffee
  end

  def update
    render :json => Coffee.all
  end

  def index
  end

  private
  def set_coffee
    @coffee = Coffee.find(params[:id])
  end
end
