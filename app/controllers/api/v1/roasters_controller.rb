class Api::V1::RoastersController < ApplicationController
  before_action :set_roaster, :only => %i(show update)
  
  def create
  end

  def show
    render :json => @roaster
  end

  def update
  end

  def index
    render :json => Roaster.all
  end

  private
  def set_roaster
    @roaster = Roaster.find(params[:id])
  end
end
