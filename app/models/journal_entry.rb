class JournalEntry < ApplicationRecord
  belongs_to :user
  belongs_to :coffee
  has_one :roaster, :through => :coffee
	belongs_to :brew_method
	
	validates :dose, :presence => true
	validates :brew_weight, :presence => true
	validates :time, :presence => true
	validates :notes, :presence => true

	def days_since_roast
		dsr = self.attributes['days_since_roast']
		if dsr == 0 || dsr == nil
			'unknown'
		else
			dsr
		end
	end
end
