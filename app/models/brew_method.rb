class BrewMethod < ApplicationRecord
	has_many :journal_entries
	has_many :coffees, :through => :journal_entries
	has_many :users, :through => :journal_entries

	validates :name, :presence => true
	validates :filter_type, :presence => true

	validates :filter_type, :inclusion => { :in => %w(Paper Metal Cloth None), :message => "must be one of 'Paper', 'Metal', 'Cloth', or 'None'" }

	def filter_type
		self.attributes['filter_type'] && self.attributes['filter_type'].capitalize
	end
end
