class Roaster < ApplicationRecord
  has_many :coffees
  has_many :journal_entries, :through => :coffees
	has_many :users, :through => :coffees
	
	validates :name, :uniqueness => true
end
