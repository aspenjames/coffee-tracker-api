class User < ApplicationRecord
	has_secure_password
	
	validates :username, :uniqueness => true, :presence => true

  has_many :journal_entries
  has_many :coffees, :through => :journal_entries
  has_many :roasters, :through => :coffees
  has_many :brew_methods, :through => :journal_entries
end
