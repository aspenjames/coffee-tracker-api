class Coffee < ApplicationRecord
  belongs_to :roaster
  has_many :journal_entries
  has_many :brew_methods, :through => :journal_entries
	has_many :users, :through => :journal_entries
	
	validates :country, :presence => true
	validates :name, :presence => true
end
