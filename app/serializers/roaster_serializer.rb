class RoasterSerializer < ActiveModel::Serializer
  attributes :id, :name
  has_many :coffees
end
