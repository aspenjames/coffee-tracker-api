class BrewMethodSerializer < ActiveModel::Serializer
  attributes :id, :name, :filter_type
end
