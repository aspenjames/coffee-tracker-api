class RecipeSerializer < ActiveModel::Serializer
  attributes :id, :dose, :brew_weight, :time, :days_since_roast, :notes
  belongs_to :user
  belongs_to :coffee
  belongs_to :brew_method
end
