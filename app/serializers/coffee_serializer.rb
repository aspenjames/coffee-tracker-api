class CoffeeSerializer < ActiveModel::Serializer
  attributes :id, :country, :name
  belongs_to :roaster
end
