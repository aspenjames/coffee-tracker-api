class UserSerializer < ActiveModel::Serializer
  attributes :id, :username
  has_many :coffees, :through => :recipes
end
