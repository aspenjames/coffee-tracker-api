# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

# Users
users = [
  {
    username: "aspen_james",
    password: "P@ssw0rd"
  },
  {
    username: "jane_doe",
    password: "P@ssw0rd"
  }
]

users.each{ |user| User.create(user) }

# Roasters
roasters = ["Seattle Coffee Works", "True North", "Broadcast Coffee", "Ladro", "Anchorhead"]

roasters.each{ |roaster| Roaster.create(name: roaster) }

# Coffees
coffees = [
  {
    country: "Ecuador",
    name: "Planada Agua Dulce"
  },
  {
    country: "Ethiopia",
    name: "Gotiti Natural"
  },
  {
    country: "Yemen",
    name: "Al Ghayoul"
  },
  {
    country: "Brazil",
    name: "Canaan Estate"
  },
  {
    country: "Blend",
    name: "Dark Side of the Moon"
  },
  {
    country: "Blend",
    name: "Drip Blend"
  },
  {
    country: "United States",
    name: "Kona"
  },
  {
    country: "Peru",
    name: "Buenos Aires"
  },
  {
    country: "Ethiopia",
    name: "Yirgacheffe Washed"
  },
  {
    country: "Guatemala",
    name: "La Bolsa"
  },
  {
    country: "Colombia",
    name: "Del Campo"
  }
]

coffees.each do |coffee|
  coffee["roaster"] = Roaster.all.sample
  Coffee.create(coffee)
end

# BrewMethods
brew_methods = [
  {
    name: "Hario V60",
    filter_type: "Paper"
  },
  {
    name: "Kalita Wave",
    filter_type: "Paper"
  },
  {
    name: "French Press",
    filter_type: "Metal"
  }
]

brew_methods.each{ |method| BrewMethod.create(method) }

# Recipes
50.times do
  JournalEntry.create({
    user: User.all.sample,
    coffee: Coffee.all.sample,
    brew_method: BrewMethod.all.sample,
    dose: rand(18..30),
    brew_weight: rand(280..480),
    time: rand(150..300),
    days_since_roast: rand(1..21),
		notes: Faker::Lorem.sentence,
		share: (rand > 0.5)
  });
end
