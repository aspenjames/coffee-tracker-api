class CreateBrewMethods < ActiveRecord::Migration[5.2]
  def change
    create_table :brew_methods do |t|
      t.string :name
      t.string :filter_type

      t.timestamps
    end
  end
end
