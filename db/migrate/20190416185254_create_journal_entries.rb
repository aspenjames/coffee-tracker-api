class CreateJournalEntries < ActiveRecord::Migration[5.2]
  def change
    create_table :journal_entries do |t|
      t.integer :user_id
      t.integer :coffee_id
      t.integer :brew_method_id
      t.integer :dose
      t.integer :brew_weight
      t.integer :time
      t.integer :days_since_roast, :default => 0
			t.text :notes
			t.boolean :share, :default => false

      t.timestamps
    end
  end
end
