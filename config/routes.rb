Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  namespace :api do
    namespace :v1 do
      resources :users, :only => %i(create show update index)

      resources :coffees, :only => %i(show update index) do
        resources :recipes, :only => %i(create show update)
      end

      get '/recipes', :to => 'recipes#index', :as => :recipes

      resources :roasters, :only => %i(create show update index)

			resources :brew_methods, :only => %i(create show update index)
			
			post '/login', :to => 'auth#create', :as => :login
			post '/signup', :to => 'users#create', :as => :signup
    end
  end
end
